import extensions
import uuid
import sys

from models.user import User
from models.token import AuthToken

from flask import request, g, Blueprint, send_from_directory
from routes.route_utilities import table_get
from routes.dbfile import download

auth = extensions.auth
db = extensions.db
dataResultSuccess = extensions.dataResultSuccess
resultSuccess = extensions.resultSuccess
resultFailure = extensions.resultFailure

user_blueprint = Blueprint("user", __name__, url_prefix="/user")


@user_blueprint.route('', methods=['GET', 'POST', 'PATCH', 'DELETE'])
def endpoint_user():
    if (request.method == 'POST'):
        return new_user()
    elif (request.method == 'GET'):
        return query_user()
    elif (request.method == 'PATCH'):
        return update_user()
    else:
        return delete_user_path()


# TODO make sure this returns user details if not admin, or add <me> path that returns the user
@auth.login_required(2)
def query_user():
    return table_get(User)


def new_user(data=None, norequest=False):  # TODO require admin status to be able to make this
    requestJson = data or request.get_json(force=True)
    username = requestJson['username']
    password = requestJson['password']
    if username is None or password is None:
        if (norequest):
            return None
        else:
            return resultFailure(msg='Missing Data', code=400)    # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        if (norequest):
            return None
        else:
            return resultFailure(msg='Existing User', code=400)    # existing user
    user = User(**requestJson)
    user.hash_password(password)
    user.guid = user.guid or uuid.uuid4()
    db.session.add(user)
    db.session.commit()
    if (norequest):
        return user
    else:
        return dataResultSuccess(user.to_dict(), code=201, spuriousParameters=list(request.args.to_dict().keys()))


@auth.login_required(1)
def update_user():  # TODO require an admin or the user himself to do this
    args = request.args.to_dict()
    body = request.get_json(force=True)
    user = None
    if (g.authLevel > 1):
        if ('guid' in args):
            guid = args.pop('guid')
            user = User.query.filter_by(guid=guid).first()
        elif ('id' in args):
            uid = args.pop('id')
            user = User.query.filter_by(id=uid).first()
        else:
            user = g.user
        if (user is None):
            resultFailure("No user found.", 404)
    else:
        user = g.user
    new_user = edit_user(user, **body)
    return dataResultSuccess(new_user.to_dict(), code=200, spuriousParameters=list(args.keys()))


def edit_user(user, **changes):
    user.set_columns(**changes)
    if ('password' in changes):
        user.hash_password(changes['password'])
        userToken = AuthToken.query.filter_by(user=user).first()
        userToken.revoked = True
    return user


@auth.login_required(1)
def delete_user_path():
    return delete_user()


#TODO deleting the user himself if he/she gets the id wrong or is not an admin is a tad bit too extreme
def delete_user(**data):
    args = request.args.to_dict() or data
    user = None
    if (g.authLevel > 1):
        if ('guid' in args):
            guid = args.pop('guid')
            user = User.query.filter_by(guid=guid).first()
        elif ('id' in args):
            uid = args.pop('id')
            user = User.query.filter_by(id=uid).first()
        else:
            user = g.user
        if (user is None):
            resultFailure("No user found.", 404)
    else:
        user = g.user
    token = AuthToken.query.filter_by(userid=user.id).first()
    db.session.delete(user)
    if (token is not None):
        db.session.delete(token)
    #TODO also delete any dbfiles related to the user
    db.session.commit()
    if (data['norequest']):
        return True
    return extensions.resultSuccess(msg="Item deleted", code=200, spuriousParameters=list(args.keys()))


@user_blueprint.route('/info')
@auth.login_required(1)
def get_user_info():
    return dataResultSuccess({
        'authlevel': g.authLevel,
        'code': g.user.code or g.user.username,
        'email': g.user.email,
        'guid': g.user.guid,
        'userid': g.user.id,
    }, spuriousParameters=list(request.args.to_dict().keys()))


@user_blueprint.route('/avatar/<int:id>')
def get_user_profile_picture(id=None):
    user = User.query.filter_by(id=id).first()
    if (user is not None):
        print(sys.path[0] + 'static/assets/images/avatar.png')
        return (download(norequest=True, code='PHOTO', masterid=user.guid) or send_from_directory(sys.path[0] + 'static/assets/images', 'avatar.png'))
