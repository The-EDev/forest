from flask import Blueprint, request, url_for, redirect
from routes.panel.panel_utilities import render_with_layout, get_datatable_actions
from routes.user import new_user, edit_user, delete_user
from routes.dbfile import upload
from models.user import User
from flask_wtf import FlaskForm
from wtforms import SubmitField, FileField, PasswordField
from wtforms.ext.sqlalchemy.orm import model_form
from extensions import auth


panel_user_blueprint = Blueprint("panel_user", __name__, url_prefix="/panel/user")

# TODO add validators
EditUserForm = model_form(model=User, base_class=FlaskForm, exclude=['guid', 'auth_token', 'created_at', 'modified_at', 'password_hash'])
EditUserForm.password = PasswordField('New Password')
EditUserForm.picture = FileField('Picture')
EditUserForm.submit = SubmitField('Edit')

CreateUserForm = model_form(model=User, base_class=FlaskForm, exclude=['guid', 'auth_token', 'created_at', 'modified_at', 'password_hash'])
CreateUserForm.password = PasswordField('Password')
CreateUserForm.picture = FileField('Picture')
CreateUserForm.submit = SubmitField('Create')
CreateUserForm.submit2 = SubmitField('Create and stay')


@panel_user_blueprint.route('', methods=['GET'])
@auth.login_required_cookie(1)
def index():
    user_head = User().get_columns()
    user_head.remove('guid')
    user_head.append('actions')
    user_head.insert(0, 'image')
    users = User.query.all()
    user_body = []
    for item in users:
        dict_to_send = item.to_dict()
        dict_to_send['actions'] = get_datatable_actions('panel_user', id=dict_to_send['id'])
        dict_to_send['image'] = '<img style="height: 40px" src="' + url_for('user.get_user_profile_picture', id=dict_to_send['id']) + '">'
        del dict_to_send['guid']
        user_body.append(dict_to_send)
    return render_with_layout("lumberjack/datatable.html.j2", headList=user_head, dataList=user_body)


@panel_user_blueprint.route('/edit/<id>', methods=['GET', 'POST'])
@auth.login_required_cookie(1)  # TODO make 2
def edit(id=None):
    user = User.query.filter_by(id=id).first()
    form = EditUserForm(obj=user)
    if (request.method == 'POST'):
        user_data = form.data
        if (user_data['password'] == ''):
            del user_data['password']
        user = edit_user(user, **user_data)
        picture = form.picture.data
        if (picture.filename != ''):
            upload([picture], 'PHOTO', masterid=user.guid)
        return redirect(url_for('.index'))
    return render_with_layout("lumberjack/edit-item.html.j2", content=form)


@panel_user_blueprint.route('/new', methods=['GET', 'POST'])
@auth.login_required_cookie(1)
def new():
    form = CreateUserForm()
    if (request.method == 'POST'):
        user_data = form.data
        user = new_user(data=user_data, norequest=True)
        picture = form.picture.data
        if (picture.filename != ''):
            upload([picture], 'PHOTO', masterid=user.guid)
        if (form.submit.data):
            return redirect(url_for('.index'))
        elif (form.submit2.data):
            return redirect(url_for('.new'))
    return render_with_layout("lumberjack/edit-item.html.j2", content=form)


@panel_user_blueprint.route('/delete/<id>', methods=['GET'])
@auth.login_required_cookie(1)
def delete(id=None):
    user = User.query.filter_by(id=id).first()
    result = delete_user(norequest=True, guid=user.guid)
    return redirect(url_for('.index'))
