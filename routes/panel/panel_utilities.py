from flask import request, render_template, url_for
from extensions import auth
from routes.route_utilities import verify_token_no_redirect


@auth.verify_token_cookie
def verify_from_cookie(token=None, authlevel=1):
    token = token or request.cookies.get('AuthToken')
    if (verify_token_no_redirect(token, authlevel)):
        return True
    else:
        return False


class menuItem():
    def __init__(self, name="", ref="", icon=""):
        self.name = name
        self.ref = ref
        self.icon = icon


class expandableMenuItem(menuItem):
    def __init__(self, name="", ref="", icon="", children=[]):
        self.name = name
        self.ref = ref
        self.icon = icon
        self.children = children


sidebarItems = [
    menuItem('users', '/panel/user', "fa fa-users"),
    #menuItem('blablas', '', 'ti-desktop'),
    #expandableMenuItem('name is name', 'javascript:void(0)', 'ti-mobile', [
    #    menuItem('users', 'panel/users', "ti-user"),
    #    menuItem('blablas', '', 'ti-desktop')
    #])
]


def render_with_layout(template, **kwargs):
    arguments = kwargs
    # injecting sidebar items
    if ('sidebarItems' not in arguments):
        arguments['sidebarItems'] = sidebarItems

    # injecting the name of the module (panel_abcd) => Abcd
    if ('name' not in arguments):
        name = request.blueprint.split('_')
        name_actual = name[1]
        arguments['name'] = name_actual.capitalize()
    return render_template(template, **arguments)


#TODO use
def get_datatable_actions(blueprint, **kwargs):
    return '<a class="btn btn-icon btn-warning" href="' + url_for(blueprint + '.edit', **kwargs) + '"><div style = "width:16px;height:16px"><i class = "fa fa-edit"></i></div></a><a class="btn btn-icon btn-danger" href="' + url_for(blueprint + '.delete', **kwargs) + '"><div style = "width:16px;height:16px"><i class = "fa fa-trash"></i></div></a>'
