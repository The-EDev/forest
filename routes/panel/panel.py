from flask import request, Blueprint, render_template, redirect, url_for, make_response
from routes.auth import post_login, logout
from extensions import auth
import json


panel_blueprint = Blueprint("panel", __name__, url_prefix="/panel")


@panel_blueprint.route('', methods=['GET'])
@auth.login_required_cookie(1)
def index():
    return redirect(url_for('panel_user.index'))


@panel_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if (request.method == "POST"):
        data = {"grant_type": "password", "username": request.form['username'], "password": request.form['password']}
        loginResult = post_login(data)
        if loginResult.status_code == 200:
            response = make_response(redirect(url_for('.index')))
            response_dict = json.loads(loginResult.data)
            response.set_cookie('AuthToken', response_dict['access_token'], samesite='Strict', max_age=response_dict['expires_in'], secure=True)
            return response
    return render_template("lumberjack/login.html")


@panel_blueprint.route('/logout', methods=['GET'])
def panel_logout():
    result = logout(norequest=True)
    if (result):
        response = make_response(redirect(url_for('.login')))
        response.delete_cookie('AuthToken')
        return response
    return redirect(url_for('.index'))
