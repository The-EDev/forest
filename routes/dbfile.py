import extensions
import os
import sys
import uuid

from flask import request, g, Blueprint, send_from_directory
from models.dbfile import DBFile
from routes.route_utilities import table_ins, table_get, table_update, table_delete

auth = extensions.auth
db = extensions.db
dataResultSuccess = extensions.dataResultSuccess
resultFailure = extensions.resultFailure

dbfile_blueprint = Blueprint("dbfile", __name__, url_prefix="/dbfile")


@dbfile_blueprint.route('', methods=['GET', 'POST', 'PATCH', 'DELETE'], endpoint='dbfile')
@auth.login_required(1)
def endpoint_dbfile():
    if (request.method == 'POST'):
        return table_ins(DBFile)
    elif (request.method == 'GET'):
        return table_get(DBFile)
    elif (request.method == 'PATCH'):
        return table_update(DBFile)
    else:
        return table_delete(DBFile)


@dbfile_blueprint.route('/load', methods=['GET', 'POST'], endpoint='dbfile_load')
@auth.login_required(1)
def load():
    # uploading
    if (request.method == 'POST'):
        uploaded_files = request.files.getlist('file')
        if (not uploaded_files):
            resultFailure("no files recieved.", 400)
        # getting all the parameters
        params = request.args.to_dict()
        masterID = None
        # removing the masterid paramerter if it is there
        if ('masterid' in params):
            masterID = params.pop('masterid')
        # if not, then use the user's guid
        else:
            masterID = g.user.guid

        code = params.pop('code') if 'code' in params else ""
        codeList = code.split(',')
        listLength = len(codeList)
        if (listLength != len(uploaded_files)):
            return resultFailure(msg="Code and file numbers mistmatch.", code=400)
        if (listLength > 1):
            code = codeList

        fileList = upload(uploaded_files, code, masterid=masterID)
        # returning the normal response + getting the non popped parameters into spurious
        return extensions.dataResultSuccess(
            fileList, count=len(fileList), spuriousParameters=list(params.keys()), code=201)

    else:  # Downloading
        download()


#[files] has to be a list (even a list of 1 item), [code] only needs to be a list if you're uploading more than one item, masterID defaults to g.user's GUID
def upload(files, code, masterid=None):
    masterid = masterid or g.user.guid
    fileList = []
    for i in range(0, len(files)):
        file = files[i]
        fileNameList = file.filename.split('.')
        filename = fileNameList[0]
        fileext = fileNameList[1]
        fileItem = DBFile.query.filter_by(masterid=masterid, code=(code[i] if type(code) is list else code)).first()
        if (fileItem is None):
            fileguid = uuid.uuid4()
            fileItem = DBFile(masterid=masterid, guid=fileguid, filename=filename, filetype=fileext, code=(code[i] if type(code) is list else code))
        else:
            os.remove(os.path.join(sys.path[0] + 'storage/dbfile', str(masterid), str(fileItem.guid) + '.' + fileItem.filetype))
            fileItem.set_columns(filename=filename, filetype=fileext)
        os.makedirs(os.path.join(sys.path[0] + "storage/dbfile", str(masterid)), exist_ok=True)
        file.save(os.path.join(sys.path[0] + 'storage/dbfile', str(masterid), str(fileItem.guid) + '.' + fileext))
        db.session.add(fileItem)
        fileList.append(fileItem.to_dict())
    db.session.commit()
    return fileList


# Norequest here means the function isn't being called from the API, which means the file is being requested for another server function
def download(norequest=False, guid=None, id=None, masterid=None, code=None):
    dbFileItem = None
    if (norequest):
        if (id is not None):
            dbFileItem = DBFile.query.filter_by(id=id.first())
        elif (guid is not None):
            dbFileItem = DBFile.query.filter_by(guid=guid).first()
        elif (code is not None and masterid is not None):
            dbFileItem = DBFile.query.filter_by(masterid=masterid, code=code).first()
        else:
            return None
    else:
        arguments = request.args.to_dict()
        if ('id' in arguments):
            dbFileItem = DBFile.query.filter_by(id=arguments['id']).first()
        elif ('guid' in arguments):
            dbFileItem = DBFile.query.filter_by(guid=arguments['guid']).first()
        elif (('code' in arguments)):
            dbFileItem = DBFile.query.filter_by(masterid=arguments['masterid'] if ('masterid' in arguments) else g.user.guid, code=arguments['code']).first()
        else:
            resultFailure("no identifier provided, you need to supply ID, GUID, or MasterID (optional) & Code.", 400)
    if (dbFileItem is None):
        if (norequest):
            return None
        else:
            resultFailure("DBFile not found.", 404)
    return send_from_directory(os.path.join(
        sys.path[0],
        'storage/dbfile',
        str(dbFileItem.masterid)),
        (str(dbFileItem.guid) + '.' + dbFileItem.filetype))
