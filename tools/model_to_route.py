import sys

# !!!!!Run this from the API's main folder (outside tools)!!!!!

# Whether login is required to access the route
needsLogin = sys.argv[2] == 'True' if len(sys.argv) > 2 else True
# The file we want to get a route for
modelFile = sys.argv[1] if len(sys.argv) > 1 else ""

if modelFile == "":
    print("usage: tools/model_to_route.py <path_to_file> <needs_login (optional True or False)>")
    sys.exit(2)

file = open(modelFile, "r")
routeFilename = file.name.split("/")[-1]

# Getting the file name without .py
blueprintNamePure = routeFilename[:-3]
# Generating a route file with the same name as the model
routeFile = open("routes/" + routeFilename, "w")

# Adding _blueprint to the name to get the blueprint variable name we'll use in the routes
blueprintName = blueprintNamePure + "_blueprint"

# The line that defines the blueprint
blueprintLine = blueprintName + ' = Blueprint("' + blueprintNamePure + '", __name__, url_prefix="/' + blueprintNamePure + '")\n'

# The initial lines to be written to the file
fileLines = [
    ("from extensions import auth\n" if needsLogin else ""),  # Only add this line if needsLogin is true
    "from flask import request, Blueprint\n",
    "from routes.route_utilities import table_get, table_ins, table_update, table_delete\n",
    "\n",
    blueprintLine,
    "\n\n",

]

# If a file has more than 1 model, this is where they'll be
classes = []

# Basically going through the input file to find all models and put them in classes list
lines = file.readlines()
for line in lines:
    if "class" in line and line.find("class") == 0:
        start = 6
        end = line.find("(")
        className = line[start:end]
        classes.append(className)

# The line to import all the models from the file
importLine = "from models." + blueprintNamePure + " import "

# The lines with the route methods
routeLines = []
for item in classes:
    importLine += item + ", "
    endpointName = item.lower()
    # If login is required, add this line to check the user's auth token
    if (needsLogin):
        routeLines.append("@auth.login_required(1)\n")

    # Adding all the endpoint (route) lines, including the default methods for each method (just replace whatever's after return for custom stuff)
    routeLines.append("@" + blueprintName + ".route(" + (("'/" + endpointName + "'") if (endpointName is not blueprintNamePure) else "''") + ", methods=['GET', 'POST', 'PATCH', 'DELETE'])\n")
    routeLines.append("def endpoint_" + endpointName + "():\n")
    routeLines.append("    if (request.method == 'POST'):\n")
    routeLines.append("        return table_ins(" + item + ")\n")
    routeLines.append("    elif (request.method == 'GET'):\n")
    routeLines.append("        return table_get(" + item + ")\n")
    routeLines.append("    elif (request.method == 'PATCH'):\n")
    routeLines.append("        return table_update(" + item + ")\n")
    routeLines.append("    else:\n")
    routeLines.append("        return table_delete(" + item + ")\n\n\n")

# Removing the last ", " from the import line before adding it to the lines to be written
importLine = importLine[:-2] + "\n"
fileLines.insert(1, importLine)

# Removing "\n\n" from the very last line
routeLines[-1] = routeLines[-1][:-2]
# Adding the route lines right after
fileLines.extend(routeLines)

# Finally flushing all the lines made onto the file we opened at the start of the script. Et voila, a route file :D
routeFile.writelines(fileLines)
