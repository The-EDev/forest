from models.base import BaseDataModel, db
from passlib.hash import pbkdf2_sha512 as sha512


class User(BaseDataModel):
    __tablename__ = 'user'
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(256))
    authLevel = db.Column(db.Integer, default=1, nullable=False)
    email = db.Column(db.String(64), unique=True)
    auth_token = db.relationship('AuthToken', back_populates='user', cascade='all, delete, delete-orphan')

    _hidden_fields = ["password_hash"]

    def hash_password(self, password):
        self.password_hash = sha512.hash(password)

    def verify_password(self, password):
        return sha512.verify(password, self.password_hash)
