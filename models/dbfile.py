from models.base import BaseDataModel, db
from sqlalchemy_utils import UUIDType


class DBFile(BaseDataModel):
    __tablename__ = 'dbfile'
    masterid = db.Column(UUIDType(binary=False))  # guid of the item that the file is attached to
    filename = db.Column(db.String(32))
    filetype = db.Column(db.String(8))  # The extension of the file (.jpg, .png)
